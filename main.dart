import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spainGreeting = "Hola Flutter";
String koreanGreeting = "헬로 플러터";
String japaneseGreeting = "ハローフラッター";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false ,
        home: Scaffold(
          appBar:AppBar(
            title: Text("Hello Flutter") ,
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting ? spainGreeting : englishGreeting;
                    });
                  } ,
                  icon: Icon(Icons.refresh)) ,

              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting ? koreanGreeting : englishGreeting;
                    });
                  } ,
                  icon: Icon(Icons.handshake)) ,

              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = displayText == englishGreeting ? japaneseGreeting : englishGreeting;
                    });
                  } ,
                  icon: Icon(Icons.face))

            ],
          ) ,
          body: Center(
            child: Text(
              displayText,
            style: TextStyle(fontSize: 24)
            ),
          ) ,
        ),
    );
  }
}

